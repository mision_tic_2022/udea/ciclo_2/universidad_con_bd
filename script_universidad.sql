
#CREAR TABLA UNIVERSIDADES
CREATE TABLE universidades(
	nit VARCHAR(10) PRIMARY KEY NOT NULL,
    nombre VARCHAR(80) NOT NULL,
    direccion VARCHAR(60) NOT NULL,
    email VARCHAR(40) NOT NULL
);

#CREAR TABLA TELEFONOS_UNIVERSIDAD
CREATE TABLE telefonos_universidad(
	id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    numero_telefono VARCHAR(15) NOT NULL,
    universidad_nit VARCHAR(10) NOT NULL,
    FOREIGN KEY(universidad_nit) REFERENCES universidades(nit)
);

#CREAR TABLA FACULTADES
CREATE TABLE facultades(
	id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    codigo VARCHAR(10) NOT NULL,
    nombre VARCHAR(60) NOT NULL,
    universidad_nit VARCHAR(10) NOT NULL,
    FOREIGN KEY(universidad_nit) REFERENCES universidades(nit)
);

#CREAR TABLA ESTUDIANTES
CREATE TABLE estudiantes(
	cedula VARCHAR(15) PRIMARY KEY NOT NULL,
    nombre VARCHAR(50) NOT NULL,
    apellido VARCHAR(50) NOT NULL,
    edad INT(2) NOT NULL,
    sexo CHAR(1) NOT NULL,
    codigo VARCHAR(10) NOT NULL,
    facultad_id INTEGER NOT NULL,
    FOREIGN KEY(facultad_id) REFERENCES facultades(id)
);

#CREAR TELEFONOS_ESTUDIANTES
CREATE TABLE telefonos_estudiante(
	id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    numero_telefono VARCHAR(15) NOT NULL,
    estudiante_cedula VARCHAR(15) NOT NULL,
    FOREIGN KEY(estudiante_cedula) REFERENCES estudiantes(cedula)
);










